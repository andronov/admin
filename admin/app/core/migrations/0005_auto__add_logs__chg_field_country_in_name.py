# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Logs'
        db.create_table(u'core_logs', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('agent', self.gf('django.db.models.fields.related.ForeignKey')(related_name='logs_agent', to=orm['core.Agent'])),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, null=True, blank=True)),
            ('type_list', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'core', ['Logs'])


        # Changing field 'Country.in_name'
        db.alter_column(u'core_country', 'in_name', self.gf('django.db.models.fields.CharField')(max_length=2))

    def backwards(self, orm):
        # Deleting model 'Logs'
        db.delete_table(u'core_logs')


        # Changing field 'Country.in_name'
        db.alter_column(u'core_country', 'in_name', self.gf('django.db.models.fields.CharField')(max_length=250))

    models = {
        u'core.agent': {
            'Meta': {'object_name': 'Agent'},
            'at': ('django.db.models.fields.IntegerField', [], {}),
            'country': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'agent_country'", 'blank': 'True', 'to': u"orm['core.Country']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'hostname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.CharField', [], {'max_length': '17'}),
            'meta': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'})
        },
        u'core.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_name': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'})
        },
        u'core.logs': {
            'Meta': {'object_name': 'Logs'},
            'agent': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'logs_agent'", 'to': u"orm['core.Agent']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'type_list': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'core.task': {
            'Meta': {'object_name': 'Task'},
            'agent_type': ('django.db.models.fields.IntegerField', [], {}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'executed': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'geo': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'task_country'", 'blank': 'True', 'to': u"orm['core.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'limit': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'loaded': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'type': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'uids': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'task_uids'", 'blank': 'True', 'to': u"orm['core.Agent']"}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['core']