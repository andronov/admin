# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Agent.hostname'
        db.add_column(u'core_agent', 'hostname',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Agent.ip'
        db.add_column(u'core_agent', 'ip',
                      self.gf('django.db.models.fields.CharField')(default=1, max_length=17),
                      keep_default=False)

        # Adding field 'Agent.created'
        db.add_column(u'core_agent', 'created',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Agent.updated'
        db.add_column(u'core_agent', 'updated',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, null=True, blank=True),
                      keep_default=False)

        # Adding M2M table for field country on 'Agent'
        m2m_table_name = db.shorten_name(u'core_agent_country')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('agent', models.ForeignKey(orm[u'core.agent'], null=False)),
            ('country', models.ForeignKey(orm[u'core.country'], null=False))
        ))
        db.create_unique(m2m_table_name, ['agent_id', 'country_id'])


    def backwards(self, orm):
        # Deleting field 'Agent.hostname'
        db.delete_column(u'core_agent', 'hostname')

        # Deleting field 'Agent.ip'
        db.delete_column(u'core_agent', 'ip')

        # Deleting field 'Agent.created'
        db.delete_column(u'core_agent', 'created')

        # Deleting field 'Agent.updated'
        db.delete_column(u'core_agent', 'updated')

        # Removing M2M table for field country on 'Agent'
        db.delete_table(db.shorten_name(u'core_agent_country'))


    models = {
        u'core.agent': {
            'Meta': {'object_name': 'Agent'},
            'at': ('django.db.models.fields.IntegerField', [], {}),
            'country': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'agent_country'", 'blank': 'True', 'to': u"orm['core.Country']"}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'hostname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.CharField', [], {'max_length': '17'}),
            'meta': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'})
        },
        u'core.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'})
        },
        u'core.task': {
            'Meta': {'object_name': 'Task'},
            'agent_type': ('django.db.models.fields.IntegerField', [], {}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'}),
            'executed': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'geo': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'task_country'", 'blank': 'True', 'to': u"orm['core.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'limit': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'loaded': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'type': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'uids': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'task_uids'", 'blank': 'True', 'to': u"orm['core.Agent']"}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['core']