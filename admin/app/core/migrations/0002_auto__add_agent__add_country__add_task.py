# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Agent'
        db.create_table(u'core_agent', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('at', self.gf('django.db.models.fields.IntegerField')()),
            ('meta', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
        ))
        db.send_create_signal(u'core', ['Agent'])

        # Adding model 'Country'
        db.create_table(u'core_country', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
            ('in_name', self.gf('django.db.models.fields.CharField')(max_length=250, blank=True)),
        ))
        db.send_create_signal(u'core', ['Country'])

        # Adding model 'Task'
        db.create_table(u'core_task', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('agent_type', self.gf('django.db.models.fields.IntegerField')()),
            ('type', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('loaded', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('executed', self.gf('django.db.models.fields.IntegerField')(blank=True)),
            ('limit', self.gf('django.db.models.fields.IntegerField')(blank=True)),
        ))
        db.send_create_signal(u'core', ['Task'])

        # Adding M2M table for field geo on 'Task'
        m2m_table_name = db.shorten_name(u'core_task_geo')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('task', models.ForeignKey(orm[u'core.task'], null=False)),
            ('country', models.ForeignKey(orm[u'core.country'], null=False))
        ))
        db.create_unique(m2m_table_name, ['task_id', 'country_id'])

        # Adding M2M table for field uids on 'Task'
        m2m_table_name = db.shorten_name(u'core_task_uids')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('task', models.ForeignKey(orm[u'core.task'], null=False)),
            ('agent', models.ForeignKey(orm[u'core.agent'], null=False))
        ))
        db.create_unique(m2m_table_name, ['task_id', 'agent_id'])


    def backwards(self, orm):
        # Deleting model 'Agent'
        db.delete_table(u'core_agent')

        # Deleting model 'Country'
        db.delete_table(u'core_country')

        # Deleting model 'Task'
        db.delete_table(u'core_task')

        # Removing M2M table for field geo on 'Task'
        db.delete_table(db.shorten_name(u'core_task_geo'))

        # Removing M2M table for field uids on 'Task'
        db.delete_table(db.shorten_name(u'core_task_uids'))


    models = {
        u'core.agent': {
            'Meta': {'object_name': 'Agent'},
            'at': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meta': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'})
        },
        u'core.country': {
            'Meta': {'object_name': 'Country'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250', 'blank': 'True'})
        },
        u'core.task': {
            'Meta': {'object_name': 'Task'},
            'agent_type': ('django.db.models.fields.IntegerField', [], {}),
            'executed': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'geo': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'task_country'", 'blank': 'True', 'to': u"orm['core.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'limit': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'loaded': ('django.db.models.fields.IntegerField', [], {'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'type': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'uids': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'task_uids'", 'blank': 'True', 'to': u"orm['core.Agent']"})
        }
    }

    complete_apps = ['core']