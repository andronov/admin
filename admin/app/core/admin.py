from django.contrib import admin

from core.models import *

class AgentAdmin(admin.ModelAdmin):
    list_display = ('at')

class TaskAdmin(admin.ModelAdmin):
    list_display = ('name')

class LogsAdmin(admin.ModelAdmin):
    list_display = ('get_type_display')

class FilesAdmin(admin.ModelAdmin):
    list_display = ('name')

admin.site.register(Agent)
admin.site.register(Task)
admin.site.register(Logs)
admin.site.register(Files)