# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.auth.decorators import login_required
from core.views import *
from django.contrib.auth.views import login, logout


urlpatterns = patterns('',
    url(r'^task/', view, name='view'),

    url(r'^test/', test, name='test'),
)