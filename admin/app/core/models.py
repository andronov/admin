# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone






class Agent(models.Model):
    type = models.IntegerField()#тип агента
    Uid = models.IntegerField(unique=True)
    country = models.CharField(max_length=2, blank=True)
    hostname = models.CharField(max_length=255, blank=True, null=True)
    ip = models.CharField(max_length=17)
    created = models.DateTimeField(default=timezone.now, blank=True, null=True)
    updated = models.DateTimeField(default=timezone.now, blank=True, null=True)

    def __unicode__(self):
        return unicode(self.at)

    class Meta:
        verbose_name = u"Агент"
        verbose_name_plural = u'Агенты'


class Task(models.Model):
    TYPE_WHO = 0
    TYPE_REP = 1
    TYPE_LIST = 2
    TYPE_FILE = 3
    TYPE_FILEM = 4
    TYPE_RE = 5
    TYPE_RD = 6

    TYPE_CHOICES = (
    (TYPE_WHO, 'who'),
    (TYPE_REP, 'rep'),
    (TYPE_LIST, 'list'),
    (TYPE_FILE, 'file'),
    (TYPE_FILEM, 'filem'),
    (TYPE_RE, 're'),
    (TYPE_RD, 'rd')
    )
    name = models.CharField(max_length=250)
    agent_type = models.IntegerField()
    type = models.IntegerField(choices=TYPE_CHOICES, blank=True, null=True)#тип задачи
    geo =  models.CharField(max_length=255, blank=True)
    uids = models.ManyToManyField(Agent, related_name='task_uids', blank=True)#Cписок идентификаторов агентов для которых установлена задача
    loaded = models.IntegerField(blank=True)#кол-во агентов забравших задачу
    executed = models.IntegerField(blank=True)#кол-во агентов выполневших задачу
    limit = models.IntegerField(blank=True)#лимит агентов на выполнение задачи
    created = models.DateTimeField(default=timezone.now,blank=True, null=True)
    updated = models.DateTimeField(default=timezone.now,blank=True, null=True)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = u"Список Задач"
        verbose_name_plural = u'Список Задач'

class Logs(models.Model):
    type = models.IntegerField(choices=Task.TYPE_CHOICES, blank=True, null=True)#тип задачи
    agent = models.ForeignKey(Agent, related_name='logs_agent')
    created = models.DateTimeField(default=timezone.now, blank=True, null=True)
    type_list = models.BooleanField(default=False, blank=True)

    def if_type_list(self):#если тип задачи список файлов,получить список файлов на сервер
        if self.type_list:
           pass

class Files(models.Model):
    name = models.CharField(max_length=250)
    path = models.CharField(max_length=350)
    size = models.IntegerField(blank=True)
    created = models.DateTimeField(default=timezone.now, blank=True, null=True)